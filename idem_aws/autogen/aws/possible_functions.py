"""List of possible functions for known operations."""

DESCRIBE_FUNCTIONS = ("get", "search", "describe")

LIST_FUNCTIONS = ("list", "describe", "search")

DELETE_FUNCTIONS = (
    "delete",
    "disassociate",
    "reject",
    "deallocate",
    "unassign",
    "deregister",
    "deprovision",
    "revoke",
    "release",
    "terminate",
    "cancel",
    "disable",
)

CREATE_FUNCTIONS = (
    "create",
    "associate",
    "accept",
    "allocate",
    "assign",
    "register",
    "provision",
    "authorize",
    "run",
    "enable",
    "upload",
    "put",
    "publish",
    "request",
    "put",
    "add",
)

UPDATE_FUNCTIONS = (
    "modify",
    "update",
)
