import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE = "aws.docdb.db_cluster"
NAME = "idem-test-doc-db-ins-" + str(int(time.time()))
PARAMETER = {
    "name": NAME,
    "engine": "docdb",
    "master_username": "admin123",
    "master_user_password": "abcd1234",
    "deletion_protection": True,
    "backup_retention_period": 7,
    "tags": {"Name": NAME},
}

comment_utils_kwargs = {"resource_type": RESOURCE_TYPE, "name": PARAMETER["name"]}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_db_cluster_create(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    ret = await hub.states.aws.docdb.db_cluster.present(ctx, **PARAMETER)
    resource = ret["new_state"]
    assert ret["result"], ret["comment"]

    if __test:
        assert would_create_message in ret["comment"]
    else:
        assert created_message in ret["comment"]

    assert not ret.get("old_state") and ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["engine"] == resource.get("engine")
    if resource.get("master_username"):
        assert PARAMETER["master_username"] == resource.get("master_username")
    assert PARAMETER["backup_retention_period"] == resource.get(
        "backup_retention_period"
    )
    assert PARAMETER["deletion_protection"] == resource.get("deletion_protection")
    assert PARAMETER["tags"] == resource.get("tags")
    if not __test and not hub.tool.utils.is_running_localstack(ctx):
        PARAMETER.pop("master_user_password", None)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present_update", depends=["present"])
async def test_db_cluster_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter.get("tags").pop("Name", None)
    new_parameter.get("tags").update(
        {
            f"idem-test-db-cluster-key-{str(int(time.time()))}": f"idem-test-db-cluster-value-{str(int(time.time()))}"
        }
    )
    new_parameter.update({"resource_id": NAME})
    new_parameter.update({"backup_retention_period": 5})
    new_parameter.update({"deletion_protection": False})

    ret = await hub.states.aws.docdb.db_cluster.present(ctx, **new_parameter)
    if __test:
        would_update_message = hub.tool.aws.comment_utils.would_update_comment(
            **comment_utils_kwargs
        )[0]
        assert would_update_message in ret["comment"]
    else:
        update_message = hub.tool.aws.comment_utils.update_comment(
            **comment_utils_kwargs
        )[0]
        assert update_message in ret["comment"][0]

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    new_state = ret["new_state"]
    assert new_parameter["deletion_protection"] == new_state.get("deletion_protection")
    assert new_parameter["tags"] == new_state.get("tags")
    assert new_parameter["backup_retention_period"] == new_state.get(
        "backup_retention_period"
    )

    if not __test and not hub.tool.utils.is_running_localstack(ctx):
        PARAMETER = copy.deepcopy(new_parameter)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present_update"])
async def test_describe(hub, ctx):
    global PARAMETER
    describe_ret = await hub.states.aws.docdb.db_cluster.describe(ctx)
    resource_id = PARAMETER["name"]
    resource = dict(
        ChainMap(*describe_ret[resource_id].get("aws.docdb.db_cluster.present"))
    )
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["engine"] == resource.get("engine")
    if resource.get("master_username"):
        assert PARAMETER["master_username"] == resource.get("master_username")
    assert PARAMETER["backup_retention_period"] == resource.get(
        "backup_retention_period"
    )
    assert PARAMETER["deletion_protection"] == resource.get("deletion_protection")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get", depends=["describe"])
async def test_get(hub, ctx):
    global PARAMETER
    get_ret = await hub.exec.aws.docdb.db_cluster.get(ctx=ctx, name=NAME)
    ret = get_ret["ret"]
    assert PARAMETER["name"] == ret.get("name")
    assert PARAMETER["engine"] == ret.get("engine")
    if ret.get("master_username"):
        assert PARAMETER["master_username"] == ret.get("master_username")
    assert PARAMETER["backup_retention_period"] == ret.get("backup_retention_period")
    assert PARAMETER["deletion_protection"] == ret.get("deletion_protection")
    assert PARAMETER["tags"] == ret.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="list", depends=["get"])
async def test_list(hub, ctx):
    global PARAMETER
    ret = await hub.exec.aws.docdb.db_cluster.list(ctx=ctx, name=NAME)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    # we expect at least one during test,
    # there could be more than one if multiple tests are running at same time
    assert len(ret["ret"]) >= 1
    db_cluster = ret["ret"][0]
    assert db_cluster.get("name")
    assert db_cluster.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.docdb.db_cluster.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["name"],
        skip_final_snapshot=True,
    )
    assert ret["result"]
    assert ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    ret["old_state"]
    if __test:
        would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
            **comment_utils_kwargs
        )[0]
        assert would_delete_message in ret["comment"]
    else:
        deleted_message = hub.tool.aws.comment_utils.delete_comment(
            **comment_utils_kwargs
        )[0]
        assert deleted_message in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.docdb.db_cluster.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        skip_final_snapshot=True,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    assert already_absent_message in ret["comment"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.docdb.db_subnet_group.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["name"]
        )
        assert (not ret["old_state"]) and (not ret["new_state"])
        assert ret["result"]
