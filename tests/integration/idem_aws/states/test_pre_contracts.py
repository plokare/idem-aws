import pytest_idem.runner as runner
import yaml

ACCT_DATA = {"profiles": {"aws": {"default": {}}}}


def test_no_sync_sls_name_and_tag(idem_cli):
    """
    Test the base case -- where no change should be made to tags at all
    """
    state = yaml.dump({"test_resource": {"aws.test.present": []}})

    with runner.named_tempfile() as fh:
        fh.write_text(state)

        ret = idem_cli("state", fh, acct_data=ACCT_DATA)

        assert ret.result, ret.stderr

        state_result = next(iter(ret.json.values()))

        assert state_result["new_state"] == {
            "name": "test_resource",
            "resource_id": None,
            "tags": None,
        }


def test_sync_sls_name_and_tag(idem_cli):
    """
    When the --sync-sls-name-and-name-tag flag is given, a tag should be created with the resources name
    """
    state = yaml.dump({"test_resource": {"aws.test.present": []}})

    with runner.named_tempfile() as fh:
        fh.write_text(state)

        ret = idem_cli("state", fh, "--sync-sls-name-and-name-tag", acct_data=ACCT_DATA)

        assert ret.result, ret.stderr

        state_result = next(iter(ret.json.values()))

        assert state_result["new_state"] == {
            "name": "test_resource",
            "resource_id": None,
            "tags": {"Name": "test_resource"},
        }


def test_sync_sls_name_and_tag_override(idem_cli):
    """
    When the --sync-sls-name-and-name-tag flag is given, and an explicit name is given,
    a tag should be created with the new name
    """
    state = yaml.dump({"test_resource": {"aws.test.present": [{"name": "new_name"}]}})

    with runner.named_tempfile() as fh:
        fh.write_text(state)

        ret = idem_cli("state", fh, "--sync-sls-name-and-name-tag", acct_data=ACCT_DATA)

        assert ret.result, ret.stderr

        state_result = next(iter(ret.json.values()))

        assert state_result["new_state"] == {
            "name": "new_name",
            "resource_id": None,
            "tags": {"Name": "new_name"},
        }


def test_sync_sls_name_and_tag_existing(idem_cli):
    """
    When the --sync-sls-name-and-name-tag flag is given, and a Name tag already exists,
    The "name" attribute should be changed to match the tag.
    """
    state = yaml.dump(
        {"test_resource": {"aws.test.present": [{"tags": {"Name": "existing"}}]}}
    )

    with runner.named_tempfile() as fh:
        fh.write_text(state)

        ret = idem_cli("state", fh, "--sync-sls-name-and-name-tag", acct_data=ACCT_DATA)

        assert ret.result, ret.stderr

        state_result = next(iter(ret.json.values()))

        assert state_result["new_state"] == {
            "name": "existing",
            "resource_id": None,
            "tags": {"Name": "existing"},
        }
