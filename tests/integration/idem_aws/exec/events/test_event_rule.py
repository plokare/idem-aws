import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_cloudwatch_rule):
    ret = await hub.exec.aws.events.rule.get(
        ctx,
        resource_id=aws_cloudwatch_rule["resource_id"],
        name=aws_cloudwatch_rule["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_cloudwatch_rule["name"] == resource.get("name")
    assert aws_cloudwatch_rule["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    rule_get_name = "idem-test-exec-get-event-rule-" + str(int(time.time()))

    ret = await hub.exec.aws.events.rule.get(
        ctx, resource_id="fake-id", name=rule_get_name
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.events.rule '{rule_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_cloudwatch_rule):
    ret = await hub.exec.aws.events.rule.list(ctx, name=aws_cloudwatch_rule["name"])
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) >= 1
    assert aws_cloudwatch_rule in ret["ret"]
