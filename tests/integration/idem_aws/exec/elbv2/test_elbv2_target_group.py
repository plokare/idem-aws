import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_id(hub, ctx):
    ret = await hub.exec.aws.elbv2.target_group.get(
        ctx,
        name="invalid-name-xyz-abc",
        resource_id="invalid-arn",
    )
    assert not ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeTargetGroups operation: "
        f"'invalid-arn' is not a valid target group ARN" in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_target_group_name(hub, ctx):
    ret = await hub.exec.aws.elbv2.target_group.get(
        ctx,
        name="invalid-name-xyz-abc",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"TargetGroupNotFoundException: An error occurred (TargetGroupNotFound) when calling the DescribeTargetGroups "
        f"operation: One or more target groups not found" in str(ret["comment"])
    )
