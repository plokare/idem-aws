import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx):
    instance_profile_get_name = "idem-test-exec-get-instance-profile-" + str(
        int(time.time())
    )
    path = "/test/"
    tags = {"Name": instance_profile_get_name}
    # Try to create the same instance profile again
    create = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_get_name,
        path=path,
        tags=tags,
    )
    assert create["result"], create["comment"]
    assert create["new_state"]
    assert instance_profile_get_name == create["new_state"].get("name")

    ret = await hub.exec.aws.iam.instance_profile.get(
        ctx,
        name=instance_profile_get_name,
        resource_id=create["new_state"]["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert instance_profile_get_name == resource.get("name")
    assert resource.get("arn")
    assert create["new_state"]["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    instance_profile_get_name = "idem-test-exec-get-instance-profile-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.iam.instance_profile.get(
        ctx,
        name=instance_profile_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"Get aws.iam.instance_profile '{instance_profile_get_name}' result is empty"
        in str(ret["comment"])
    )
