==========
Quickstart
==========

Get started with **idem-aws** in 5 minutes.

Idem is an Apache licensed project for managing complex cloud environments.

Idem-aws is an Idem plugin to allow you to manage many parts of the AWS cloud.


Supported AWS resources
+++++++++++++++++++++++

.. grid:: 2

    .. grid-item-card:: Idem Exec Modules
        :link: /ref/exec/index
        :link-type: doc

        Exec Modules

        :bdg-info:`AWS`

    .. grid-item-card:: Idem State Modules
        :link: /ref/states/index
        :link-type: doc

        State Modules

        :bdg-info:`AWS`



The first step is to :doc:`install Idem and Idem-aws. </quickstart/install/index>`

.. toctree::
   :maxdepth: 1
   :glob:
   :hidden:

   install/index
   configure/index
   commands/index
   more/index
